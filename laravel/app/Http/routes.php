<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@home')->name('home');
    Route::get('autorizacao', 'HomeController@autorizacao')->name('autorizacao');
    Route::post('autorizacao', 'HomeController@autorizacaoPost')->name('autorizacao.post');
    Route::get('ficha-de-saude', 'HomeController@saude')->name('saude');
    Route::get('o-que-levar', 'HomeController@levar')->name('levar');
    Route::get('mapa', 'HomeController@mapa')->name('mapa');
    Route::get('fotos', 'HomeController@fotos')->name('fotos');

    Route::get('chegando', function() {
        return view('frontend.chegando');
    });

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::resource('autorizacoes', 'AutorizacoesController');
        Route::resource('fotos', 'FotosController');
        Route::resource('usuarios', 'UsuariosController');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
