<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AutorizacaoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filho_nome' => 'required',
            'filho_rg'   => 'required',
            'mae_nome'   => 'required',
            'mae_rg'     => 'required',
            'pai_nome'   => 'required',
            'pai_rg'     => 'required',
        ];
    }
}
