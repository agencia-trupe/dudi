<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FotosRequest;
use App\Http\Controllers\Controller;

use App\Models\Foto;
use App\Helpers\CropImage;

class FotosController extends Controller
{
    private $image_config = [
        [
            'width'   => 200,
            'height'  => 200,
            'path'    => 'assets/img/fotos/thumbs/'
        ],
        [
            'width'   => 800,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/fotos/'
        ]
    ];

    public function index()
    {
        $imagens = Foto::ordenados()->get();

        return view('painel.fotos.index', compact('imagens'));
    }

    public function show(Foto $imagem)
    {
        return $imagem;
    }

    public function store(FotosRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = Foto::create($input);

            $view = view('painel.fotos.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar foto: '.$e->getMessage();

        }
    }

    public function destroy(Foto $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.fotos.index')
                             ->with('success', 'Foto excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir foto: '.$e->getMessage()]);

        }
    }
}
