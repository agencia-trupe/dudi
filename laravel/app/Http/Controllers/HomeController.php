<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\AutorizacoesRequest;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function home()
    {
        return view('frontend.home');
    }

    public function autorizacao()
    {
        return view('frontend.autorizacao');
    }

    public function autorizacaoPost(AutorizacoesRequest $request)
    {
        \App\Models\Autorizacao::create($request->all());

        \Mail::send('emails.autorizacao', $request->all(), function($message) use ($request)
        {
            $message
                ->to('barmitzvahdudi@gmail.com', 'Bar Mitzvah Dudi')
                ->from('noreply@trupe.net', 'Bar Mitzvah Dudi')
                ->subject('Autorização de viagem');
        });

        return back()->with('success', true);
    }

    public function saude()
    {
        return view('frontend.saude');
    }

    public function levar()
    {
        return view('frontend.levar');
    }

    public function mapa()
    {
        return view('frontend.mapa');
    }

    public function fotos()
    {
        $fotos = \App\Models\Foto::ordenados()->get();
        return view('frontend.fotos', compact('fotos'));
    }
}
