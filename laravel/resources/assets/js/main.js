(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.conviteHome = function() {
        var $convite = $('.convite');
        if (!$convite.length) return;

        var $imagem = $convite.find('img');

        $convite.waitForImages(function () {
            posicionaImagem();
            $imagem.addClass('animated lightSpeedIn').delay(100).css('opacity', 1);
        });

        var posicionaImagem = function () {
            $imagem.css('margin-left', - $imagem.width() / 2);
            $imagem.css('margin-top', - $imagem.height() / 2);
        };

        var slideConvite = function () {
            $convite.addClass('animated slideOutUp').delay(200).fadeOut();
        };

        $convite.click(slideConvite);
        $(window).on('wheel mousewheel scroll', slideConvite);
        $(window).on('resize', posicionaImagem);
        $(window).trigger('resize');
    };

    App.fotos = function() {
        $('.fancybox').fancybox({
            padding: 10
        });
    };

    App.chegando = function() {
        $('.chegando').waitForImages(function() {
            $('.chegando h1').addClass('animated lightSpeedIn').delay(100).css('opacity', 1);
            $('.chegando img').addClass('animated rubberBand').delay(100).css('opacity', 1);

            setTimeout(function() {
                $('.bg').addClass('animated flipInY');
                $('.texto h2, h3').addClass('animated slideInLeft');
                $('.texto p').addClass('animated slideInRight');

                $('body').css('height', 'auto');
            }, 500);
        });
    };

    App.init = function() {
        this.conviteHome();
        this.fotos();
        this.chegando();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
