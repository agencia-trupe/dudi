<!DOCTYPE html>
<html>
<head>
    <title>[DUDI] Autorização de viagem</title>
    <meta charset="utf-8">
</head>
<body>
    <p>
        Autorizamos nosso filho(a)<br>
        <strong>{{ $filho_nome }} / RG {{ $filho_rg }}</strong><br>
        a viajar desacompanhado(a) ao Acampamento NR nos dias 1, 2 e 3 de julho de 2016.
    </p>

    <p>
        Mãe:<br>
        <strong>{{ $mae_nome }} / RG {{ $mae_rg }}</strong>
    </p>

    <p>
        Pai:<br>
        <strong>{{ $pai_nome }} / RG {{ $pai_rg }}</strong>
    </p>
</body>
</html>