<ul class="nav navbar-nav">
    <li @if(str_is('painel.autorizacoes.*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.autorizacoes.index') }}">Autorizações</a>
    </li>
    <li @if(str_is('painel.fotos.*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.fotos.index') }}">Fotos</a>
    </li>
</ul>
