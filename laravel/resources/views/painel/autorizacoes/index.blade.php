@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Autorizações</h2>
    </legend>

    <table class="table table-striped table-bordered table-hover">
        <tbody>
        @foreach ($autorizacoes as $autorizacao)
            <tr class="tr-row">
                <td>
                    <a href="{{ route('painel.autorizacoes.show', $autorizacao->id) }}">
                        {{ $autorizacao->filho_nome }}
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
