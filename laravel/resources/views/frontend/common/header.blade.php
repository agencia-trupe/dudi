    <header>
        <a href="{{ route('home') }}" class="dudi"></a>

        <div class="center">
            <nav>
                <a href="{{ route('autorizacao') }}" @if(Route::currentRouteName() == 'autorizacao') class="active" @endif>Autorização de viagem</a>
                <a href="{{ route('saude') }}" @if(Route::currentRouteName() == 'saude') class="active" @endif>Ficha de Saúde</a>
                <a href="{{ route('levar') }}" @if(Route::currentRouteName() == 'levar') class="active" @endif>O que levar?</a>
                <a href="{{ route('mapa') }}" @if(Route::currentRouteName() == 'mapa') class="active" @endif>Mapa</a>
                <a href="{{ route('fotos') }}" @if(Route::currentRouteName() == 'fotos') class="active" @endif>Galeria de fotos</a>
            </nav>
        </div>
    </header>
