@extends('frontend.common.template')

@section('content')

    <div class="main fotos center">
        @foreach($fotos as $foto)
        <a href="{{ asset('assets/img/fotos/'.$foto->imagem) }}" class="fancybox" rel="fotos">
            <img src="{{ asset('assets/img/fotos/thumbs/'.$foto->imagem) }}" alt="">
        </a>
        @endforeach
    </div>

@endsection
